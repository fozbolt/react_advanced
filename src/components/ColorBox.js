import React, { useContext } from 'react'
import { colorBoxContext } from '../App'

function ColorBox() {
    const initialSize = useContext(colorBoxContext)

    const initialHeight = 100
    const initialWidth = 100

    const themeStyles = {
        backgroundColor: initialSize ? 'black' : 'green',
        padding: '20px',
        margin: '20px',
        height: initialSize ? `${initialHeight}px` : `${initialHeight + 10 }px`, 
        width: initialSize ? `${initialWidth}px` : `${initialWidth + 10 }px`
    }
    
        
    return (
        <div style = {themeStyles}>  </div>
    )
}

export default ColorBox
