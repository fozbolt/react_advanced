import React, { Component } from 'react'

export class ListItem extends Component {
 
    render() {
        const users = this.props.users;
        const listItems = users.map((user,index) =>    <li key={index}>  {user.userName}, {user.age}, {user.registrationDate}</li>  );  
  
    return (
        <ul>{listItems}</ul>  );
            
        }
}

export default ListItem
