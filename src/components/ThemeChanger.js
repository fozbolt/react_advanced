import React, { useContext } from 'react'
import { ThemeContext, FontContext } from '../App';


function ThemeChanger() {
    
    const darkTheme = useContext(ThemeContext)
    const bigFont = useContext(FontContext)

    const themeStyles = {
        backgroundColor: darkTheme ? 'black' : 'white',
        color: darkTheme? 'white' : 'black',
        padding: '20px',
        margin: '20px'
    }

    const fontStyles = {
        fontSize: bigFont? '18px' : '12px'
    }



  return (
    <div style = {themeStyles}> 
        <h3>Random text</h3>
        <p style= {fontStyles}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum maximus 
            rhoncus nisl, eget aliquet mauris finibus quis. Curabitur tempor egestas velit,
            a egestas tortor eleifend et. Praesent tempus nulla ac ligula dapibus mollis. 
            Maecenas mollis risus non metus tempor, ut blandit risus sodales. Nulla ex libero,
            elementum quis libero sodales, accumsan hendrerit dolor. Vivamus pulvinar aliquet 
            hendrerit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere
            cubilia curae; Morbi a ultrices ex. Nulla eu turpis sem. Aliquam euismod nisl eu dui
            finibus, eget pellentesque leo tempor. Curabitur luctus, augue vel ultricies venenatis, 
            ante neque placerat sapien, ac egestas. 
        </p>
    </div>
      
  )
}

export default ThemeChanger
