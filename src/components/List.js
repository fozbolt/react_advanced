import React, { Component } from 'react'
import ListItem from './ListItem'

export class List extends Component {
    state = {

        users : [
            {
                userName: 'filjet1',
                age: 20,
                registrationDate: '12/12/2021'
            },
            {
                userName: 'filjet2',
                age: 30,
                registrationDate: '12/02/2021'
            },
            {
                userName: 'filjet3',
                age: 31,
                registrationDate: '01/12/2020'
            },
            {
                userName: 'filjet4',
                age: 33,
                registrationDate: '05/05/2005'
            },
            {
                userName: 'filjet5',
                age: 66,
                registrationDate: '12/05/21'
            }

        ],

        //us : this.state.users.map((user, index) => ( {id : index, ...user})) 

        
      
    }
    render() {
        return (
            <div>
                <ListItem users = {this.state.users}  />
            
                <h5> user count: {this.state.users.length}</h5>
            </div>
        )
    }
}

export default List
