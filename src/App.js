import List from './components/List'
import ThemeChanger from './components/ThemeChanger'
import React, {useState} from 'react';
import ColorBox from './components/ColorBox';

//Task 2
//da ga mozemo koristiti u ostatku aplikacije
export const ThemeContext = React.createContext();
export const FontContext = React.createContext();

//Tasl 3
export const colorBoxContext = React.createContext();

function App() {
  //task 4
  const [darkTheme, setDarkTheme] = useState(false)
  const [bigFont, setBigFont] = useState(false)

  //task 5
  const [initialSize, setInitialSize] = useState(true)


  function toggleTheme(){
    setDarkTheme(prevDarkTheme => !prevDarkTheme)
  }

  function toggleFont(){
    setBigFont(prevBigFont => !prevBigFont)
  }

  function toggleInitialSize(){
    //da se izvede samo jednom
    setInitialSize(prevInitalSize => !prevInitalSize)
  }

  return (
    <div className="App">
      <h2>Task 1 - User listing mechanism:</h2>
        <List />

      <h2>Task 2 - Theme changer:</h2>
  
        <ThemeContext.Provider value={darkTheme}>
          <FontContext.Provider value={bigFont}>
            <button onClick={toggleTheme}>  Toggle theme</button>
            <button onClick={toggleFont}>  Increase size </button>
          </FontContext.Provider>

          <ThemeChanger />
        </ThemeContext.Provider>

        <h2>Task 3 - Color Box:</h2>
          <colorBoxContext.Provider value={initialSize}>
            <button onClick={toggleInitialSize}>  Toggle big font</button>
            <ColorBox />
          </colorBoxContext.Provider>
         
      
    </div>
  );
}

export default App;
